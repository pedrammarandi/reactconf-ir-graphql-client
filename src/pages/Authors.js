import React from "react";
import { useAuthorsWithPosts } from "./../api/author/hooks";
import { A } from "hookrouter";

function Authors() {
  const { data, error, loading } = useAuthorsWithPosts();
  if (loading) return "Still Loading";
  if (error) return "Error darim ghorbonet beram man";

  return (
    <div>
      <ul>
        {data.allAuthors.map((author, index) => (
          <>
            <li key={index}>{author.name}</li>
            <ol>
              {author.Posts.map((post, index) => (
                <li key={index}>
                  <A href={`/posts/${post.id}`}>{post.title}</A>
                </li>
              ))}
            </ol>
          </>
        ))}
      </ul>
    </div>
  );
}

export default Authors;
