import React from "react";
import { usePost } from "./../api/post/hooks";

function Post({ id }) {
  const { loading, error, data } = usePost({ id });
  if (loading) return "Loading ....";
  if (error) return "Error!";

  return (
    <div>
      <h2>{data.Post.title}</h2>
      <span> {data.Post.Author.name} </span>
    </div>
  );
}

export default Post;
