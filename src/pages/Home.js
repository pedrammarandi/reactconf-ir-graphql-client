import React from "react";

function Home() {
  return (
    <div>
      <h2> Home </h2>
      <hr />
      <span>
        <b> Do you want to make yours? Install the below packages </b>
      </span>
      <pre>npm install apollo-boost @apollo/react-hooks graphql</pre>
    </div>
  );
}

export default Home;
