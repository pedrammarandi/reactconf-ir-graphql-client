import React from "react";
import { A } from "hookrouter";
import { usePosts } from "./../api/post/hooks";

function Posts() {
  const { loading, error, data } = usePosts();
  const { allPosts } = data;
  if (loading) return "Loading ...";
  if (error) return "Error";

  return (
    <div>
      <h3> Posts </h3>
      <ul>
        {allPosts.map((post, key) => (
          <A href={`/posts/${post.id}`}>
            <li key={key}> {post.title} </li>{" "}
          </A>
        ))}
      </ul>
    </div>
  );
}

export default Posts;
