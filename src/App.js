import React from "react";
import { useRoutes } from "hookrouter";
import { ApolloProvider } from "@apollo/react-hooks";
import PageWrapper from "./components/PageWrapper";
import routes from "./lib/routes";
import client from "./lib/apollo";

function App() {
  const routeResult = useRoutes(routes);
  return (
    <ApolloProvider client={client}>
      <PageWrapper> {routeResult} </PageWrapper>
    </ApolloProvider>
  );
}

export default App;
