import React from "react";
import Home from "./../pages/Home";
import Posts from "./../pages/Posts";
import Post from "./../pages/Post";
import Authors from "./../pages/Authors";

export default {
  "/": () => <Home />,
  "/posts": () => <Posts />,
  "/authors": () => <Authors />,
  "/posts/:id": ({ id }) => <Post id={id} />
};
