import { useQuery } from "@apollo/react-hooks";
import { ALL_AUTHORS_WITH_POSTS } from "./queries.graphql";

function useAuthorsWithPosts() {
  return useQuery(ALL_AUTHORS_WITH_POSTS);
}

export { useAuthorsWithPosts };
