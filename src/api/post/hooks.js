import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { GET_ALL_POSTS } from "./queries.graphql"; //Graphql document
import { GET_POST } from "./queries.graphql";

function usePosts() {
  return useQuery(GET_ALL_POSTS);
}

function usePost({ id }) {
  return useQuery(GET_POST, {
    fetchPolicy: "network-only",
    variables: {
      id
    }
  });
}

export { usePosts, usePost };
