import React from "react";
import { navigate } from "hookrouter";

import styled from "styled-components";

const Sidebar = () => {
  const route = address => navigate(address);
  return (
    <StyledSidebar>
      <ul>
        <li onClick={() => route("/")}>Home</li>
        <li onClick={() => route("/posts")}>Posts</li>
        <li onClick={() => route("/authors")}>Authors</li>
      </ul>
    </StyledSidebar>
  );
};

const StyledSidebar = styled("div")`
  display: flex;
  ming-height: 280px;
  width: 200px;
  margin-right: 30px;
  padding: 5px;

  ul {
    list-style: none;
    width: 100%;
    margin: 0;
    padding: 0;
    cursor: pointer;

    li {
      margin: 10px 0;
      background: #fff;
      color: #050238;
      width: 100%;
      padding: 5px;
      border-radius: 5px;
    }
  }
`;

export default Sidebar;
