import React from "react";
import styled, { createGlobalStyle } from "styled-components";
import Sidebar from "./Sidebar";

function PageWrapper(props) {
  return (
    <div>
      <GlobalStyle />
      <h1> React + Apollo + Hooks </h1>
      <StyledPageWrapper>
        <Sidebar />
        {props.children}
      </StyledPageWrapper>
    </div>
  );
}

export default PageWrapper;

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
  ul {
    list-style: none;
    margin: 0;
    padding: 0;
    li {
      margin: 5px 0;
    }
  }
  body {
    min-height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  h1 {
    color:  #e50a7c;
  }
  h2 {
    color: #050238;
  }
  pre {
    border-radius: 4px;
    background: #f3f3f3;
    padding: 5px;
  }
`;

const StyledPageWrapper = styled("div")`
  position: relative;
  display: flex;
  background-color: #e5e5e5;
  min-width: 80vw;
  min-height: 300px;
  color: #050238;
  box-sizing: border-box;
  padding: 20px;
  border-radius: 5px;
`;
